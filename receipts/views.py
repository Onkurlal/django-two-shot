from django.shortcuts import render, redirect, get_object_or_404
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm

# Create your views here.
@login_required
def home(request):
    receipts = Receipt.objects.filter(purchaser = request.user)
    context = {
        'receipts': receipts
    }
    return render(request, 'receipts/list_receipts.html', context)

@login_required
def create_receipt(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('home')

    else:
        form = ReceiptForm()
    context = {
        'form': form,
    }
    return render(request, 'receipts/create_receipt.html', context)

@login_required
def category_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    receipts_by_expense = {}
    for receipt in receipts:
        if receipt.category in receipts_by_expense:
            receipts_by_expense[receipt.category] += 1
        else:
            receipts_by_expense[receipt.category] = 1
    context={
        'receipts_by_expense':receipts_by_expense
    }
    return render(request, 'receipts/category_list.html',context)

@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    receipts = Receipt.objects.filter(purchaser = request.user)
    receipts_by_account = {}
    for receipt in receipts:
        if receipt.account in receipts_by_account:
            receipts_by_account[receipt.account] += 1

        else:
            receipts_by_account[receipt.account] = 1
    context={
        'accounts': accounts,
        'receipts_by_account': receipts_by_account
    }
    return render(request, 'receipts/account_list.html', context)

@login_required
def create_category(request):
    if request.method == 'POST':
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect('category_list')
    else:
        form = ExpenseCategoryForm()
    context={
        'form': form,
    }
    return render(request, 'receipts/create_category.html', context)

@login_required
def create_account(request):
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect('account_list')
    else:
        form = AccountForm()
    context =  {
        'form': form,
    }
    return render(request, 'receipts/create_account.html', context)
